import styled from "@emotion/styled";
import HamburgerIcon from "../atoms/icon/hamburger";
import PayIcon from "../atoms/icon/pay";
import ChatIcon from "../atoms/icon/chat";
import Alarm from "../atoms/icon/alarm";

const Wrap = styled.div`
  display: flex;
  justify-content: space-between;
  padding-top: 20px;
`;
const LeftWrap = styled.div`
  display: flex;
`;

const RightWrap = styled.div`
  display: flex;
`;

const HeaderTop = {
  MenuAndCard: () => {
    return (
      <LeftWrap>
        <HamburgerIcon />
        <PayIcon />
      </LeftWrap>
    )
  },
  ChatAndAlarm: () => {
    return (
      <RightWrap>
        <ChatIcon />
        <Alarm />
      </RightWrap>
    )
  }
}

const HeaderLogoContainer = () => {
  return (
    <Wrap>
      <HeaderTop.MenuAndCard />
      <HeaderTop.ChatAndAlarm />
    </Wrap>
  )
}

export default HeaderLogoContainer;