import { FC } from "react";
import styled from "@emotion/styled";
import NaverIcon from "../atoms/icon/naverLogo";
import ServiceItemComponent from "../atoms/headerServiceItem";
import { ServiceItemProp } from "../organisms/header";

const Wrap = styled.div`
  display: flex;
  justify-content: center;
`;

const InnerWrap = styled.div`
  width: 676px;
  margin: 72px auto 0;
  height: 100px;
  position: relative;
`;

const HeaderBottom = {
  ServiceListBundle: ({ serviceItemList }: { serviceItemList: ServiceItemProp[] }) => {
    return (
      <InnerWrap>
        <ServiceItemComponent serviceItemList={serviceItemList} />
      </InnerWrap>
    )
  }
}

const HeaderServiceContainer: FC<{ serviceItemList: ServiceItemProp[] }> = ({ serviceItemList }) => {
  return (
    <Wrap>
      <HeaderBottom.ServiceListBundle serviceItemList={serviceItemList} />
    </Wrap>
  )
}

export default HeaderServiceContainer;