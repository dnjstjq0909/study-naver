import styled from "@emotion/styled";

import LeftContentsBodyRight from "./leftContentsBodyRight";
import LeftContentsBodyLeft from "./leftContentsBodyLeft";

const Wrap = styled.div`
  display: flex;
  margin: 18px 20px;
  width: 790px;
  height: 224px;
  
`;

const LeftContentsBody = () => {
  return (
    <Wrap>
      <LeftContentsBodyLeft />
      <LeftContentsBodyRight />
    </Wrap>
  )
}

export default LeftContentsBody;