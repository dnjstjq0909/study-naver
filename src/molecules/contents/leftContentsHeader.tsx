import styled from "@emotion/styled";
import LeftContentsHeaderText from "../../atoms/contents/leftContentsHeaderText";
import Slash from "../../atoms/slash";
import React, { FC } from "react";

interface LeftContentHeaderProps {
  headerTextArray: string[];
  onClickHeaderText: (name: string) => void;
  contentsStatus: string;
  selected: boolean;
}

const Wrap = styled.div`
  display: flex;
  padding: 18px 20px 17px;
  position: relative;
`;

const LeftContentHeader: FC<LeftContentHeaderProps> = ({ headerTextArray, onClickHeaderText, contentsStatus, selected }) => {
  return (
    <Wrap>
      {
        headerTextArray.map((name, index) => {
          return (
            <React.Fragment key={index}>
              <LeftContentsHeaderText name={name} onClickHeaderText={() => onClickHeaderText(name)} contentsStatus={contentsStatus} selected={selected}></LeftContentsHeaderText>
              <Slash display={index === 0 ? "none" : "block"} />
            </React.Fragment>
          )
        })
      }
    </Wrap>
  )
}

export default LeftContentHeader;