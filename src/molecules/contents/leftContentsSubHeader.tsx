import styled from "@emotion/styled";
import React, { FC } from "react";
import { sportsText2Array, sportsTextArray } from "../../fakeData/subHeaderTextArray";

import LinkMoveText from "../../atoms/linkMoveText";
import Bar from "../../atoms/bar";
import { font } from "../../style/style";

interface SubHeaderProps {
  contentsStatus?: string;
}

const Wrap = styled.div`
  display: flex;
  background-color: #f5f7f8;
  margin: 0 20px;
  padding: 12px 20px;
  border-radius: 4px;
  line-height: 26px;
`;

const Left = styled.div`
  display: flex;
  margin-right: auto;
`;
const Right = styled.div`
  display: flex;
`;

const WrapBetween = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
`;

const Li = styled.li`
  ::before {
    color: #d3d5d7;
  }
`;

const LeftContentsSubHeader: FC<SubHeaderProps> = ({ contentsStatus }) => {

  return (
    <Wrap>
      {
        contentsStatus === "스포츠" && (
          <WrapBetween>
            <Left>
              {
                sportsTextArray.map((text, index) => {
                  return (
                    <React.Fragment key={index}>
                      <Li>
                        <LinkMoveText name={text}></LinkMoveText>
                      </Li>
                    </React.Fragment>
                  )
                })
              }
            </Left>

            <Right>
              {
                sportsText2Array.map((text, index) => {
                  return (
                    <React.Fragment key={index}>
                      <LinkMoveText name={text} weight={index === 1 ? font.weight.bold : ""}></LinkMoveText>
                      <Bar display={index === 0 ? "block" : "none"} />
                    </React.Fragment>
                  )
                })
              }
            </Right>

          </WrapBetween>
        )
      }
    </Wrap>
  )
}

export default LeftContentsSubHeader;