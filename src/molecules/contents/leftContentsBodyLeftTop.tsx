import styled from "@emotion/styled";
import { font } from "../../style/style";
import { Dispatch, SetStateAction, useEffect, useState } from "react";
import ThumbNailData from "../../fakeData/articleThumbnail";


const Wrap = styled.div`
  display: flex;
  width: 385px;
  height: 104px;
  justify-content: center;
`;

const TeamScoreBundle = styled.div`
  display:flex;
  width: 154px;
  justify-content: center;
`;

const ScoreLeagueBundle = styled.div``;

const TeamBundle = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin: 0 16px;
`;

const TeamThumbnail = styled.div`
  width: 52px;
  height: 52px;
  border: 1px solid #000;
  line-height: 1.5;
`;

const TeamName = styled.span`
  margin-top: 4px;
  font-weight: ${font.weight.semiBold};
  font-size: ${font.size.small};
`;

const Score = styled.span`
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: ${font.size.xLarge};
  font-weight: ${font.weight.black};
  width: 52px;
`;

const LeagueInfo = styled.div`
  width: 77px;
  height: 104px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  
`;

const SetInfo = styled.div`
  background-color: #f4361e;
  display: inline-block;
  max-width: 100%;
  margin-top: 3px;
  padding: 0 6px;
  border-radius: 11px;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  font-size: ${font.size.small};
  line-height: 20px;
  font-weight: bold;
  vertical-align: top;
  color: #fff;
`;

const LeagueName = styled.div`
  color: #1f65ef;
  line-height: 1.5;
  font-size: ${font.size.small};
  font-weight: ${font.weight.bold};
  margin-top: 8px;
`;

const LeftContents = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
`;

const MediaList = styled.div`
  display: flex;
  align-items: center;
  justify-content: start;
  :nth-child(2) {
    margin-top: 16px;
  }
`;

const ThumbnailContainer = styled.div`
  overflow: hidden;
  position: relative;
`;

const ThumbnailWrapper = styled.div<{ articleHover: boolean, url: string }>`
  width: 185px;
  height: 104px;
  margin-right: 16px;
  background-image: url(${props => props.url});
  background-repeat: no-repeat;
  background-position: center;
  background-size: cover;
  cursor: pointer;
  transition: transform 0.3s ease;
  transform: ${(props) => props.articleHover ? "scale(1.05)" : "scale(1)"};
`;

const TitleBundle = styled.div`
  font-weight: ${font.weight.semiBold};
`;

const Title = styled.div`
  color: #101010;
  font-size: ${font.size.size15};
  width: 176px;
  max-height: 42px;
  display: -webkit-box;
  -webkit-line-clamp: 2;
  -webkit-box-orient: vertical;
  overflow: hidden;
  text-overflow: ellipsis;
  cursor: pointer;
  :hover {
    text-decoration: underline;
  }
`;

const SubTitle = styled.div`
  color: #606060;
  margin-top: 7px;
  font-size: ${font.size.size13};
`;

const RightContents = styled.div`
`;


const GameStatus = {
  DuringTheGame: () => {
    return (
      <>
        <TeamScoreBundle>
          <TeamBundle>
            <TeamThumbnail></TeamThumbnail>
            <TeamName>한국도로공사</TeamName>
          </TeamBundle>
          <Score>0</Score>
        </TeamScoreBundle>
        <ScoreLeagueBundle>
          <LeagueInfo>
            <SetInfo>2세트
            </SetInfo>
            <LeagueName>V-리그</LeagueName>
          </LeagueInfo>
        </ScoreLeagueBundle>
        <TeamScoreBundle>
          <Score>2</Score>
          <TeamBundle>
            <TeamThumbnail></TeamThumbnail>
            <TeamName>GS칼텍스</TeamName>
          </TeamBundle>
        </TeamScoreBundle>
      </>
    )
  }
}

const Article = {
  ThumbnailArticle: () => {
    const [mediaListHover, setMediaListHover] = useState(Array(ThumbNailData.length).fill(false));

    const handleMouseEnter = (index: number) => {
      const newMediaListHover = [...mediaListHover];
      newMediaListHover[index] = true;
      setMediaListHover(newMediaListHover);
    };

    const handleMouseLeave = (index: number) => {
      const newMediaListHover = [...mediaListHover];
      newMediaListHover[index] = false;
      setMediaListHover(newMediaListHover);
    };

    return (
      <LeftContents>
        {
          ThumbNailData.map((list, index) => {
            return (
              <MediaList
                onMouseEnter={() => handleMouseEnter(index)}
                onMouseLeave={() => handleMouseLeave(index)}
              >
                <ThumbnailContainer>
                  <ThumbnailWrapper articleHover={mediaListHover[index]} url={list.url}></ThumbnailWrapper>
                </ThumbnailContainer>
                <TitleBundle>
                  <Title>{list.title}</Title>
                  <SubTitle>{list.subTitle}</SubTitle>
                </TitleBundle>
              </MediaList>
            )
          })
        }

      </LeftContents>
    )


  },
  ListArticle: () => {
    return (
      <RightContents>

      </RightContents>
    )
  }
}

const LeftContentsBodyLeftTop = () => {
  return (
    <Wrap>
      {/* <GameStatus.DuringTheGame /> */}
      <Article.ThumbnailArticle></Article.ThumbnailArticle>
    </Wrap>
  )
}

export default LeftContentsBodyLeftTop;