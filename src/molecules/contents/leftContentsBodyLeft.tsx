import styled from "@emotion/styled";
import LeftContentsBodyLeftTop from "./leftContentsBodyLeftTop";
import LeftContentsBodyLeftBottom from "./LeftContentsBodyLeftBottom";

const Wrap = styled.div``;



const LeftContentsBodyLeft = () => {
  return (
    <Wrap>
      <LeftContentsBodyLeftTop />
      <LeftContentsBodyLeftBottom />
    </Wrap>
  )
}

export default LeftContentsBodyLeft;