import styled from "@emotion/styled"
import NaverIcon from "../atoms/icon/naverLogo";
import { font } from "../style/style";

const Wrap = styled.div`
  display: flex;
  justify-content: center;
`;

const InnerWrap = styled.div`
  width: 710px;
  /* background-color: red; */
  position: relative;
`;

const InputWrap = styled.div`
  position: absolute;
  left: 50%;
  height: 58px;
  z-index: 10;
  width: 706px;
  border: 1px solid #03c75a;
  border-radius: 33px;
  background-color: #fff;
  transform: translateX(-50%);
`;

const InputLeftLogoContainer = styled.div`
  padding-left: 10px;
  display: flex;
`;

const SearchInput = styled.input`
  width: 70%;
  border: none;
  outline: none;
  font-size: ${font.size.xLarge};
  font-weight: ${font.weight.bold};
`;

const HeaderMiddle = {
  InputBundle: () => {
    return (
      <InnerWrap>
        <InputWrap>
          <InputLeftLogoContainer>
            <NaverIcon />
            <SearchInput />
          </InputLeftLogoContainer>
        </InputWrap>
      </InnerWrap>
    )
  }
}

const HeaderSearchContainer = () => {
  return (
    <Wrap>
      <HeaderMiddle.InputBundle />
    </Wrap>
  )
}

export default HeaderSearchContainer;