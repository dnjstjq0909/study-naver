import styled from "@emotion/styled";
import { useEffect, useState } from "react";
import LeftContentHeader from "../molecules/contents/leftContentsHeader";
import LeftContentsSubHeader from "../molecules/contents/leftContentsSubHeader";
import LeftContentsBody from "../molecules/contents/leftContentsBody";

const Wrap = styled.div`
  display: flex;
  flex-direction: column;
  width: 830px;
  height: 436px;
  border: 1px solid #ebebeb;
  border-radius: 10px;
  margin-top: 16px;
`;

const LeftContents = () => {
  const [headerTextArray, setHeaderTextArray] = useState<string[]>([]);
  const [contentsStatus, setContentsStatus] = useState<string>("");
  const [selected, setSelected] = useState<boolean>(false);


  useEffect(() => {
    setHeaderTextArray(["뉴스스탠드", "언론사편집", "엔터", "스포츠", "경제"]);
  }, []);


  const onClickHeaderText = (name: string) => {
    setContentsStatus(name);
    console.log(contentsStatus, "클릭")
  }

  return (
    <Wrap>
      <LeftContentHeader headerTextArray={headerTextArray} onClickHeaderText={onClickHeaderText}
        contentsStatus={contentsStatus} selected={selected}></LeftContentHeader>
      <LeftContentsSubHeader contentsStatus={contentsStatus}></LeftContentsSubHeader>
      <LeftContentsBody></LeftContentsBody>
    </Wrap>
  )
}

export default LeftContents;