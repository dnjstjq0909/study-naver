import styled from "@emotion/styled";
import HeaderLogoContainer from "../molecules/headerLogoContainer";
import HeaderSearchContainer from "../molecules/headerSearchContainer";
import HeaderServiceContainer from "../molecules/headerServiceContainer";
import { useEffect, useState } from "react";
import { itemList } from "../fakeData/headerServiceItem";

export interface ServiceItemProp {
  iconUrl: string;
  name: string;
}

const Wrap = styled.div`
  display: flex;
  flex-direction: column;
`;

const HeaderComponent = () => {
  const [serviceItemList, setServiceItemList] = useState<ServiceItemProp[]>([]);

  useEffect(() => {
    setServiceItemList(itemList)
  }, [])

  return (
    <Wrap>
      <HeaderLogoContainer />
      <HeaderSearchContainer />
      <HeaderServiceContainer serviceItemList={serviceItemList} />
    </Wrap >
  )
}

export default HeaderComponent;