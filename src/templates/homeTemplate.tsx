import styled from "@emotion/styled";
import HeaderComponent from "../organisms/header";
import AddBanner from "../atoms/addBanner";
import LeftContents from "../organisms/leftContents";

const Wrap = styled.div``;

const BodyWrap = styled.div`
  display: flex;
`;

const LeftWrap = styled.div``;

const RightWrap = styled.div``;

const HomeTemplate = () => {
  return (
    <Wrap>
      <HeaderComponent />
      <BodyWrap>
        <LeftWrap>
          <AddBanner />
          <LeftContents />
        </LeftWrap>
        <RightWrap></RightWrap>
      </BodyWrap>
    </Wrap>
  )
}

export default HomeTemplate;