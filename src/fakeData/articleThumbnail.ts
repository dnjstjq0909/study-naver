const ThumbNailData = [
  {
    url: `/images/image1.jpg`,
    title: `누리카드 경기 3초전 극적으로 역전!`,
    subTitle: `스포티비뉴스`
  },
  {
    url: `/images/image3.jpeg`,
    title: `'홍콩전 노쇼' 메시, 일본 고베전에는 뛸까..."몸 상태를 지켜봐야"`,
    subTitle: `스포티비뉴스`
  },
]

export default ThumbNailData;