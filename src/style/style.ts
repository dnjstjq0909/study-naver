export const theme = {
  color: {
    dark: "#383c3c",
    white: "#fff"
  }
}

export const font = {
  size: {
    xSmall: "10px",
    small: "13px",
    medium: "16px",
    large: "18px",
    xLarge: "24px",
    "size10": "10px",
    "size11": "11px",
    "size12": "12px",
    "size13": "13px",
    "size14": "14px",
    "size15": "15px",
    "size16": "16px",
    "size17": "17px",
    "size18": "18px",
    "size19": "19px",
    "size20": "20px",
    "size21": "21px",
    "size22": "22px",
    "size23": "23px",
    "size24": "24px",

  },
  weight: {
    lithgt: "300",
    normal: "400",
    medium: "500",
    semiBold: "600",
    bold: "700",
    extraBold: "800",
    black: "900",
  }
}