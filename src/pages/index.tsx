import styled from '@emotion/styled';
import HomeTemplate from '../templates/homeTemplate';

const Wrap = styled.div`
  display: flex;
  justify-content: center;
`;
const Container = styled.div`
  width: 1280px;
  max-width: 1280px;
`;

const HomePage = () => {

  return (
    <Wrap>
      <Container>
        <HomeTemplate />
      </Container>
    </Wrap>
  )
}

export default HomePage;