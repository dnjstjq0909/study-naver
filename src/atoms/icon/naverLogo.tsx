import styled from "@emotion/styled";

const Wrap = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 58px;
  height: 58px;
`;

const NaverIcon = () => {
  return (
    <Wrap>
      <svg role="img" fill="#03c75a" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
        <title>Naver</title>
        <path d="M16.273 12.845 7.376 0H0v24h7.726V11.156L16.624 24H24V0h-7.727v12.845Z" /></svg>
    </Wrap>
  )
}
export default NaverIcon;