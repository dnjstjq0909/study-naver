
import styled from "@emotion/styled";

const Wrap = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 54px;
  height: 54px;
`;

const ThreeDotIcon = () => {
  return (
    <Wrap>
      <svg fill="none" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
        <path d="M4 11H4" stroke="black" stroke-linecap="round" stroke-linejoin="round" stroke-width="4" />
        <path d="M12 11H12" stroke="black" stroke-linecap="round" stroke-linejoin="round" stroke-width="4" />
        <path d="M20 11H20" stroke="black" stroke-linecap="round" stroke-linejoin="round" stroke-width="4" />
      </svg>
    </Wrap>
  )
}
export default ThreeDotIcon;