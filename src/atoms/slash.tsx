import styled from "@emotion/styled";
import { FC } from "react";

interface WrapProps {
  display: string;
}

const Wrap = styled.div<WrapProps>`
  content: "";
  display: ${(props) => props.display};
  width: 1px;
  height: 15px;
  background-color: #d3d5d7;
  transform: skewX(-15deg);
  margin: 2px 8px 0px 8px;
`;
const Slash: FC<WrapProps> = ({ display }) => {
  return (
    <Wrap display={display}></Wrap>
  )
}

export default Slash;