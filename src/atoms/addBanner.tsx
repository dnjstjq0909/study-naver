import styled from "@emotion/styled";

const Wrap = styled.div`
  width: 830px;
  height: 130px;
  aspect-ratio: auto 830 / 130;
  border: 1px solid #ebebeb;
  border-radius: 10px;
`;

const AddBanner = () => {
  return (
    <Wrap></Wrap>
  )
}
export default AddBanner;