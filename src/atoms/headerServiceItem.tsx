import styled from "@emotion/styled";
import { font } from "../style/style";
import { FC } from "react";
import { ServiceItemProp } from "../organisms/header";
import ThreeDotIcon from "./icon/threeDot";

const Wrap = styled.div`
  display: flex;
`;

const ItemWrap = styled.div`
  width: 64px;
  margin-left: 4px;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const ItemIconInner = styled.div`
  width: 54px;
  height: 54px;
  border-radius: 30%;
  border: 1px solid #ebebeb;
`;

const ItemName = styled.div`
  margin-top: 7px;
  line-height: 1.5;
  font-size: ${font.size.medium};
  font-weight: ${font.weight.semiBold};
`;

const Item = {
  ItemIcon: ({ serviceItemList }: { serviceItemList: ServiceItemProp[] }) => {
    return (
      <>
        {serviceItemList.map((item, index) => {
          return (
            <ItemWrap>
              <ItemIconInner>{item.iconUrl}</ItemIconInner>
              <ItemName>{item.name}</ItemName>
            </ItemWrap>
          )
        })}
        <ItemWrap>
          <ItemIconInner>
            <ThreeDotIcon />
          </ItemIconInner>
        </ItemWrap>
      </>
    )
  }
}

const ServiceItemComponent: FC<{ serviceItemList: ServiceItemProp[] }> = ({ serviceItemList }) => {

  return (
    <Wrap>
      <Item.ItemIcon serviceItemList={serviceItemList} />
    </Wrap>
  )
}

export default ServiceItemComponent;