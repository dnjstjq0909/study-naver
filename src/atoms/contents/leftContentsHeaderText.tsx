import styled from "@emotion/styled";
import { FC } from "react";
import { font } from "../../style/style";

interface NameProps {
  selected: boolean;
}

interface LeftContentsHeaderTextProps {
  name: string;
  onClickHeaderText: (name: string) => void;
  contentsStatus: string;
  selected: boolean;
}

const Name = styled.span<NameProps>`
  color: ${props => props.selected ? "#000" : "rgba(8,8,8,.5)"};
  text-decoration: none;
  cursor: pointer;
  font-size: 16.8px;
  font-family: system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
  :hover {
    text-decoration: underline;
  }
  font-weight: ${font.weight.bold};
`;

const LeftContentsHeaderText: FC<LeftContentsHeaderTextProps> = (
  { name, onClickHeaderText, contentsStatus }
) => {
  return (
    <>
      <Name onClick={() => onClickHeaderText(name)} selected={contentsStatus === name}>{name}</Name>
    </>
  )
}

export default LeftContentsHeaderText;