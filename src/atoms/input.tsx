import { ChangeEvent, FC } from "react";

interface Props {
  type: string;
  value: string;
  width?: string;
  name?: string;
  disabled?: boolean;
  placeholder?: string;
  readOnly?: boolean;
  checked?: boolean;
  onChange?: (event: ChangeEvent<HTMLInputElement>) => void;
}

const Input: FC<Props> = ({
  type,
  value,
  width,
  name,
  disabled,
  placeholder,
  readOnly,
  checked,
  onChange,
}) => {

  return (
    <input type={type} value={value} width={width} name={name} disabled={disabled}
      placeholder={placeholder} readOnly={readOnly} checked={checked} onChange={onChange} />
  )
}

export default Input;