import styled from "@emotion/styled";
import { FC } from "react";

interface LinkMoveTextProps {
  name: string;
  weight?: string;
}

interface TextProps {
  weight?: string;
}

const Text = styled.a<TextProps>`
  font-weight: ${props => props.weight};
  cursor: pointer;
`;

const LinkMoveText: FC<LinkMoveTextProps> = ({ name, weight }) => {
  return (
    <Text weight={weight}>{name}</Text>
  )
}

export default LinkMoveText;